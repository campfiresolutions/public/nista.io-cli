[![Gitlab Pipeline](https://gitlab.com/campfiresolutions/public/gnista.io-cli/badges/master/pipeline.svg)](https://gitlab.com/campfiresolutions/public/gnista.io-cli/-/pipelines)  [![Python Version](https://img.shields.io/pypi/pyversions/gnista-cli)](https://pypi.org/project/gnista-cli/)  [![PyPI version](https://img.shields.io/pypi/v/gnista-cli)](https://pypi.org/project/gnista-cli/)  [![License](https://img.shields.io/pypi/l/gnista-cli)](https://pypi.org/project/gnista-cli/)  [![Downloads](https://img.shields.io/pypi/dm/gnista-cli)](https://pypi.org/project/gnista-cli/) 

# gnista-cli
A cli for accessing gnista.io

# Installing
Install gnista-cli for all Users, remove `-a` if you want to install for your current user only
``` bash
pip install gnista-cli
python -m gnista_cli i -a
```

## Links
**Website**
[![gnista.io](https://www.gnista.io/assets/images/gnista-logo-small.svg)](https://gnista.io)

**PyPi**
[![PyPi](https://pypi.org/static/images/logo-small.95de8436.svg)](https://pypi.org/project/gnista-cli/)

**GIT Repository**
[![Gitlab](https://about.gitlab.com/images/icons/logos/slp-logo.svg)](https://gitlab.com/campfiresolutions/public/gnista.io-cli)