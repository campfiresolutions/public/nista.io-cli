import json
import logging
import socket
import uuid
from datetime import datetime
from typing import Optional, Union

import dateutil.parser
import structlog
from gnista_library.gnista_connetion import KeyringGnistaConnection
from signalrcore.hub.auth_hub_connection import AuthHubConnection
from signalrcore.hub.base_hub_connection import BaseHubConnection
from signalrcore.hub_connection_builder import HubConnectionBuilder

from gnista_cli.influxdb_import import InfluxDbImport

log = structlog.getLogger()


class GnistaWebsocketConnection(KeyringGnistaConnection):

    hub_connection: Union[BaseHubConnection, AuthHubConnection]

    def __init__(self, workspace_id: str, base_url: Optional[str] = None):
        super().__init__(workspace_id=workspace_id, base_url=base_url)
        self.scope = ["data-api", "websocket-api"]
        self.agent_id = str(uuid.UUID(int=uuid.getnode()))

    def connect_agent(self):
        self.hub_connection = HubConnectionBuilder()\
            .with_url(self.base_url + "/api/ws/hubs/agent",
                      options={
                          "access_token_factory": super().get_access_token,
                          "verify_ssl": False,
                      })\
            .configure_logging(logging.INFO, handler=structlog.get_logger())\
            .with_automatic_reconnect({
                "type": "raw",
                "keep_alive_interval": 10,
                "reconnect_interval": 5,
                "max_attempts": 5,
                "allowReconnect": True
            }).build()

        self.hub_connection.on_open(self._handle_opened)
        self.hub_connection.on_close(lambda: print("connection closed"))
        self.hub_connection.on_error(lambda e: log.error("error on websocket", error=e.__dict__, exec_info=True))
        self.hub_connection.on("ReceiveMessage", print)
        self.hub_connection.on("ExecuteTask", self._execute_task)
        self.hub_connection.start()

        message = ""
        while message != "exit()":
            message = input(">> ")

        self.hub_connection.stop()

    def _handle_opened(self):
        host = socket.gethostname()
        log.info("connection opened and handshake received ready to send messages", host=host)
        self.hub_connection.send("RegisterAgent", [self.agent_id, self.workspace_id, "windows", host])

    def _update_schema(self, values: dict):
        connection_string = values["ConnectionString"]
        data_source_id = values["DataSourceId"]
        influx_import = InfluxDbImport(workspace_id=self.workspace_id, gnista_url=self.base_url, influx_host=connection_string)
        influx_import.update_schema(data_source_id=data_source_id)

    def _read_influx(self, task_content: dict):
        data_point_id = task_content["DataPointId"]
        connection_string = task_content["ConnectionString"]
        data_base_name = task_content["DataBaseName"]
        measurement = task_content["Measurement"]
        field_name = task_content["FieldName"]
        workspace_id = task_content["WorkspaceId"]

        load_from_string = task_content["LoadFrom"]
        load_from: Union[None, datetime] = None

        if load_from_string:
            load_from = dateutil.parser.isoparse(load_from_string)

        load_to_string = task_content["LoadTo"]

        if load_to_string:
            load_to = dateutil.parser.isoparse(load_to_string)

        influx_import = InfluxDbImport(gnista_url=self.base_url, influx_host=connection_string, workspace_id=workspace_id)
        influx_import.start_import(database_name=data_base_name, measurement_name=measurement,
                                   data_point_id=data_point_id, field_name=field_name, start_range=load_from, stop_range=load_to)

    def _execute_task(self, values: dict):
        log.info("Received Execute Task")
        task_id = values[0]
        try:
            task_content = json.loads(values[1])
            name = task_content["Name"]
            if name == "UpdateSchemaTask":
                self._update_schema(task_content)
            if name == "ReadInfluxByField":
                self._read_influx(task_content)
# pylint:disable=W0703
        except Exception as ex:
            log.error(ex)
            self.hub_connection.send("TaskFinished", [self.agent_id, self.workspace_id, task_id, values[1], str(ex)])

        self.hub_connection.send("TaskFinished", [self.agent_id, self.workspace_id, task_id, values[1], None])
