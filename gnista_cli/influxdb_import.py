import datetime
import random
import re
import time
import uuid
from typing import List, Optional, Union
from uuid import UUID

import pandas as pd
from attrs import define
from data_point_client.models.problem_details import ProblemDetails
from data_point_client.types import Response
from data_source_client.models.influx_db_import_task_config import \
    InfluxDbImportTaskConfig
from data_source_client.models.update_auto_detect_task_configs_request import \
    UpdateAutoDetectTaskConfigsRequest
from gnista_library import (GnistaConnection, GnistaDataPoint,
                            GnistaDataSource, KeyringGnistaConnection,
                            StaticTokenGnistaConnection)
from gnista_library.gnista_data_source import AutoDetectTaskConfig
from influxdb import InfluxDBClient
from structlog import get_logger

log = get_logger()


@define
class InfluxDbImport:
    _DATE_NAME = "Date"
    _VALUE_NAME = "Value"

    gnista_url: str
    workspace_id: str
    influx_host: Optional[str] = "localhost"
    influx_port: Optional[int] = 8086
    influx_username: Optional[str] = "root"
    influx_password: Optional[str] = "root"
    refresh_token: Optional[str] = None

    _influx_client: InfluxDBClient = None
    _connection: GnistaConnection = None

    def __attrs_post_init__(self):
        if self.refresh_token is not None:
            self._connection = StaticTokenGnistaConnection(
                workspace_id=self.workspace_id, refresh_token=self.refresh_token, base_url=self.gnista_url, verify_ssl=False)
        else:
            self._connection = KeyringGnistaConnection(workspace_id=self.workspace_id, base_url=self.gnista_url, verify_ssl=False)

        self._influx_client = InfluxDBClient(host=self.influx_host, port=self.influx_port, username=self.influx_username,
                                             password=self.influx_password, ssl=False, verify_ssl=False)

    def update_schema(self, data_source_id: UUID):
        databases = self._influx_client.get_list_database()

        log.info("Connecting to Influx and searching for Databases")
        auto_task_configs = []
        for database in databases:
            this_database_name = database["name"]
            if this_database_name == "_internal":
                continue
            auto_task_configs.extend(self._update_database_schema(this_database_name))
        data_source = GnistaDataSource(self._connection, data_source_id=data_source_id)
        request = UpdateAutoDetectTaskConfigsRequest(auto_detect_task_configs=auto_task_configs)
        response = data_source.update_auto_detect_task_configs(request)

        if isinstance(response, Response):
            if response.status_code not in (200, 202):
                log.error(response)
                raise Exception("Received an error code: " + str(response.status_code))

        if isinstance(response, ProblemDetails):
            log.error(response)
            raise Exception(response.detail)

    def _update_database_schema(self, database_name: str) -> List[AutoDetectTaskConfig]:
        self._influx_client.switch_database(database_name)
        measurements = self._influx_client.get_list_measurements()
        auto_task_configs = []
        for measurement in measurements:
            measurement_name = measurement["name"]
            auto_task_configs.extend(self._update_measurement_schema(database_name=database_name, measurement_name=measurement_name))
        return auto_task_configs

    def _update_measurement_schema(self, database_name: str, measurement_name: str) -> List[AutoDetectTaskConfig]:
        field_keys = self._influx_client.query("SHOW FIELD KEYS FROM \"" + measurement_name + "\"")
        auto_detect_task_configs = []
        for field_key in field_keys[measurement_name]:
            field_key_name = field_key["fieldKey"]
            name = database_name + " " + measurement_name + " " + field_key_name
            tags = re.split(r"\.|>", field_key_name)
            import_task_config = InfluxDbImportTaskConfig(
                discriminator="InfluxDbImportTaskConfig",
                name=name, tags=tags, measurement=measurement_name, database=database_name, field_name=field_key_name)
            task_config = AutoDetectTaskConfig(value_header=name, import_task_config=import_task_config)
            auto_detect_task_configs.append(task_config)
        return auto_detect_task_configs

    def _check_database(self, database_name: str):
        databases = self._influx_client.get_list_database()
        for database in databases:
            this_database_name = database["name"]
            log.info(this_database_name + "\r\n")
            if this_database_name == database_name:
                return True
        return False

    def _check_measurement(self, measurement_name: str):
        measurements = self._influx_client.get_list_measurements()
        for measurement in measurements:
            this_measurement_name = measurement["name"]
            if this_measurement_name == measurement_name:
                return True

        return False

    def _check_field_name(self, measurement_name: str, field_name: str):
        field_keys = self._influx_client.query("SHOW FIELD KEYS FROM " + measurement_name)
        for field_key in field_keys[measurement_name]:
            this_field_key_name = field_key["fieldKey"]
            if this_field_key_name == field_name:
                return True
        return False

    def start_simulation(self, database_name: str, measurement_name: str):
        log.info("Connecting to Influx and searching for Databases")

        if not self._check_database(database_name=database_name):
            self._influx_client.create_database(database_name)

        self._influx_client.switch_database(database_name)
        id_tag = str(uuid.uuid4())

        start_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        delta = datetime.timedelta(seconds=5)
        end_date = datetime.datetime.now().timestamp() * 1000
        time_stamp = start_date.timestamp() * 1000

        while time_stamp < end_date:
            data = []
            for _ in range(0, 1000):
                data.append(f"{measurement_name},id_tag={id_tag} value={random.randint(0, 50)} {int(time_stamp)}")
                time_stamp = time_stamp + (delta.total_seconds()*1000)

            self._influx_client.write_points(data, database=database_name, time_precision='ms', batch_size=1000, protocol='line')

        while True:
            time_stamp = int(time.time_ns() / 1000000)
            data = []
            data.append(f"{measurement_name},id_tag={id_tag} value={random.randint(0, 50)} {time_stamp}")

            self._influx_client.write_points(data, database=database_name, time_precision='ms', batch_size=2, protocol='line')
            time.sleep(1)

# pylint:disable=R0913
    def _load_data(
        self,
        start_range: Union[datetime.datetime, None],
        stop_range: datetime.datetime,
        measurement_name: str,
        field_name: str,
        limit: int,
        offset: int
    ) -> List:
        if start_range:
            start_range_string = "time >= '" + start_range.isoformat() + "' AND "
        else:
            start_range_string = ""

        query = "SELECT \"" + field_name + "\" FROM " + measurement_name + " WHERE " + start_range_string + \
            "time <= '" + stop_range.isoformat() + "' LIMIT " + str(limit) + " OFFSET " + str(offset)

        log.info("Running Query: %s", query)

        data = self._influx_client.query(query, epoch="ms")
        data2 = data.get_points()
        offset = offset + limit

        record = []
        for point in data2:
            time_column = pd.to_datetime(point["time"], unit='ms').to_pydatetime()
            record.append({InfluxDbImport._DATE_NAME: time_column, InfluxDbImport._VALUE_NAME: point[field_name]})
        return record

    def _upload_data(
        self,
        data_point: GnistaDataPoint,
        record: List
    ):
        data_frame = pd.DataFrame.from_records(record, columns=[InfluxDbImport._DATE_NAME, InfluxDbImport._VALUE_NAME])

        data_frame = data_frame.set_index(data_frame[InfluxDbImport._DATE_NAME])
        data_frame = data_frame.drop([InfluxDbImport._DATE_NAME], axis=1)

        if isinstance(data_point, GnistaDataPoint):
            response = data_point.append_data_point_data(
                data=[data_frame],
                unit=None)

            if isinstance(response, Response):
                if response.status_code not in (200, 202):
                    log.error(response)
                    raise Exception("Received an error code: " + str(response.status_code))

            if isinstance(response, ProblemDetails):
                log.error(response)
                raise Exception(response.detail)

    def start_import(
            self,
            database_name: str,
            measurement_name: str,
            field_name: str,
            data_point_id: UUID,
            start_range: Union[datetime.datetime, None],
            stop_range: datetime.datetime):

        log.info("Connecting to Influx and searching for Databases")

        if not self._check_database(database_name=database_name):
            log.error("Database not found")
            raise Exception("Database not found")

        self._influx_client.switch_database(database_name)

        log.info("database found")

        if not self._check_measurement(measurement_name=measurement_name):
            log.error("Measurement not found")
            raise Exception("Measurement not found")

        log.info("Measurement found")

        if not self._check_field_name(measurement_name=measurement_name, field_name=field_name):
            log.error("Field not found")
            raise Exception("Field not found")

        log.info("Field found")

        data_point = GnistaDataPoint(self._connection, data_point_id)

        limit = 5000
        offset = 0
        while True:
            record = self._load_data(start_range=start_range, stop_range=stop_range, measurement_name=measurement_name,
                                     field_name=field_name, limit=limit, offset=offset)

            offset = offset + limit

            if not record:
                break

            if len(record) == 0:
                break

            self._upload_data(data_point=data_point, record=record)

            if len(record) < (limit - 1):
                break
